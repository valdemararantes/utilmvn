package com.galgo.utils;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by valdemar.arantes on 19/05/2016.
 */
public class ConfigTest {

    private static final Logger log = LoggerFactory.getLogger(ConfigTest.class);

    @Test
    public void testReadWrite() throws Exception {
        final File f = new File(this.getClass().getResource("/config-tests.properties").toURI());
        Config conf = new Config(f);
        log.debug("chave1={}", conf.getProperty("chave1"));

        conf.setProperty("chave2", "valor2");
        conf.save();
    }
}