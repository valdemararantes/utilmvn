/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils;

import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

/**
 *
 * @author valdemar.arantes
 */
@Ignore("Classe utilizada para validar se a classe testada foir implementada corretamente. Não" +
        "Não deve ser testada a cada build.")
public class PropertyUtilsTest {

    public static class Pessoa {

        private String nome;
        private List<Pessoa> pais = Lists.newArrayList();
        private Address endereco;

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public Address getEndereco() {
            return endereco;
        }

        public void setEndereco(Address endereco) {
            this.endereco = endereco;
        }

        public List<Pessoa> getPais() {
            return pais;
        }

        public void setPais(List<Pessoa> pais) {
            this.pais = pais;
        }



        @Override
        public String toString() {
            StringBuffer paisStr = null;
            if (pais != null && !pais.isEmpty()) {
                paisStr = new StringBuffer("[");
                for (Pessoa pai : pais) {
                    paisStr.append(pai).append(", ");
                }
                paisStr.setLength(paisStr.length() - 2);
                paisStr.append("]");
            }
            return "Pessoa{" + "nome=" + nome + ", pais=" + paisStr + ", endereco=" + endereco + '}';
        }

    }

    public static class Address {

        private String logradouro;

        public String getLogradouro() {
            return logradouro;
        }

        public void setLogradouro(String logradouro) {
            this.logradouro = logradouro;
        }

        @Override
        public String toString() {
            return "Address{" + "logradouro=" + logradouro + '}';
        }

    }

    private Pessoa neto;

    public PropertyUtilsTest() {
    }

    @Before
    public void setUp() {
        neto = new Pessoa();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void setSimpleProperty() {
        try {
            PropertyUtils.setProperty(neto, "nome", "Neto");
            System.out.println("neto=" + ToStringBuilder.reflectionToString(neto));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void setNestedProperty() {
        try {
            PropertyUtils.setProperty(neto, "endereco.logradouro", "Rua Heitor Peixoto");
            System.out.println("neto=" + neto);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void setIndexedProperty() {
        try {
            PropertyUtils.setProperty(neto, "pais[0].", "Júlia");
            System.out.println("neto=" + neto);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
