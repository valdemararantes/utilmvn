/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils.xml;

import com.google.common.io.Resources;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * @author valdemar.arantes
 */
public class SchemaValidatorTest {

    private static final Logger log = LoggerFactory.getLogger(SchemaValidatorTest.class);
    private static URL ESCOLA_XSD_URL;
    private static URL EXTRATO_XSD_URL;
    private static URL ISO_REDUZIDO_XSD_URL;

    @BeforeClass
    public static void initClass() {
        ESCOLA_XSD_URL = Resources.getResource("escola.xsd");
        EXTRATO_XSD_URL = Resources.getResource(
                "Galgo_Extrato_de_Conciliacao_de_Cotas_XSD_Envio_e_Reenvio_camt.043.001.03.xsd");
        ISO_REDUZIDO_XSD_URL = Resources.getResource("ISO_Reduzido.xsd");
    }

    //@Test
    public void validate() {
        try {
            List<String> errors = SchemaValidator.
                    validate(new File("src/test/resources/escola.xml"),
                            ESCOLA_XSD_URL);
            log.info("errors={}", toString(errors));
            Assert.assertEquals(0, errors.size());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail("Exceção inesperada");
        }
    }

    //@Test
    public void validate_1tagNaoExistente() {
        try {
            List<String> errors = SchemaValidator.validate(
                    new File("src/test/resources/escola_1-tag-nao-existente.xml"),
                    ESCOLA_XSD_URL);
            log.info("errors={}", toString(errors));
            Assert.assertEquals(1, errors.size());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail("Exceção inesperada");
        }
    }

    //@Test
    public void validate_NonExistentFile() {
        try {
            List<String> errors = SchemaValidator.validate(
                    new File("src/test/resources/escola_xml-nao-existente.xml"),
                    ESCOLA_XSD_URL);
            log.info("errors={}", toString(errors));
            Assert.assertEquals(1, errors.size());
        } catch (IllegalArgumentException e) {
            log.info(e.getMessage());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail("Exceção inesperada");
        }
    }

    //@Test
    public void validate_extrato_teste() {
        try {
            List<String> errors = SchemaValidator.validate(
                    new File("src/test/resources/Extrato_teste.xml"),
                    EXTRATO_XSD_URL);
            log.info("errors={}", toString(errors));
            Assert.assertEquals(0, errors.size());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail("Exceção inesperada");
        }
    }

    //@Test
    public void validate_extrato_consolidado() {
        try {
            List<String> errors = SchemaValidator.validate(
                    new File("src/test/resources/01-Exemplo_Arquivo_Envio_Extrato_Consolidado.xml"),
                    EXTRATO_XSD_URL);
            log.info("errors={}", toString(errors));
            Assert.assertEquals(0, errors.size());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail("Exceção inesperada");
        }
    }

    //@Test
    public void validate_extrato_consolidado_usando_iso_reduzido() {
        try {
            List<String> errors = SchemaValidator.validate(
                    new File("src/test/resources/01-Exemplo_Arquivo_Envio_Extrato_Consolidado.xml"),
                    ISO_REDUZIDO_XSD_URL);
            log.info("errors={}", toString(errors));
            Assert.assertEquals(0, errors.size());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail("Exceção inesperada");
        }
    }

    //@Test
    public void validate_regras_estrutura3_neto_usando_iso_reduzido() {
        try {
            Locale originalLocale = Locale.getDefault();
            Locale.setDefault(Locale.ENGLISH);
            List<String> errors = SchemaValidator.validate(
                    new File(
                            "K:/Condominio_STI/001 - Sist Galgo/Documentacao/04 Testes/Cenarios Galgo/"
                                    + "Extrato/EV/Validador/ID13/regras_estrutura3_neto.xml"),
                    ISO_REDUZIDO_XSD_URL);
            Locale.setDefault(originalLocale);
            log.info("errors={}", toString(errors));
            Assert.assertEquals(1, errors.size());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail("Exceção inesperada");
        }
    }

    //@Test
    public void validate_regras_estrutura2_usando_iso_reduzido() {
        try {
            List<String> errors = SchemaValidator.validate(
                    new File(
                            "K:/Condominio_STI/001 - Sist Galgo/Documentacao/04 Testes/Cenarios Galgo/"
                                    + "Extrato/EV/Validador/ID13/regras_estrutura1_neto.xml"),
                    ISO_REDUZIDO_XSD_URL);
            log.info("errors={}", toString(errors));
            Assert.assertEquals(1, errors.size());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail("Exceção inesperada");
        }
    }

    private String toString(List<String> errors) {
        if (errors == null || errors.isEmpty()) {
            return "";
        }
        StringBuilder buff = new StringBuilder();
        for (String error : errors) {
            buff.append(error).append("\n");
        }
        return buff.toString();
    }

}
