/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class ClassUtilsTest {

    private static final Logger log = LoggerFactory.getLogger(ClassUtilsTest.class);

    class Pessoa {

        List<String> apelidos;
        List amigos;
    }

    @Test
    public void genericListTest() {
        Assert.assertTrue(ClassUtils.isListField(Pessoa.class, "apelidos"));
    }

    @Test
    public void notGenericListTest() {
        Assert.assertTrue(ClassUtils.isListField(Pessoa.class, "amigos"));
    }

    @Test
    public void getGenericTypeFromGenericListField() {
        Assert.assertEquals(String.class, ClassUtils.getGenericTypeFromField(Pessoa.class, "apelidos"));
    }

    @Test
    public void getGenericTypeFromNotGenericListField() {
        Assert.assertNull(ClassUtils.getGenericTypeFromField(Pessoa.class, "amigos"));
    }
}
