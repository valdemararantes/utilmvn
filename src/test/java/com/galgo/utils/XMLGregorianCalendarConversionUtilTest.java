/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils;

import java.text.ParseException;
import javax.xml.datatype.XMLGregorianCalendar;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class XMLGregorianCalendarConversionUtilTest {

    private static final Logger log = LoggerFactory.getLogger(
            XMLGregorianCalendarConversionUtilTest.class);

    @Test
    public void asXMLGregorianCalendar_Date_UsingString() {
        try {
            XMLGregorianCalendar xmlGregDate = XMLGregorianCalendarConversionUtil.
                    asXMLGregorianCalendar_Date("28-03-2014",
                            "dd-MM-yyyy");
            log.debug("xmlGregDate={}", xmlGregDate.toXMLFormat());
        } catch (ParseException e) {
            log.error(null, e);
        }
    }

    @Test
    public void asXMLGregorianCalendar_UsingString() {
        try {
            XMLGregorianCalendar xmlGregDate = XMLGregorianCalendarConversionUtil.
                    asXMLGregorianCalendar("28-03-2014",
                            "dd-MM-yyyy");
            log.debug("xmlGregDate={}", xmlGregDate.toXMLFormat());
        } catch (ParseException e) {
            log.error(null, e);
        }
    }
}
