/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.net;

import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author valdemar.arantes
 */
public class Proxy {
	private String address;
	private int port;
	private String user;
	private String password;

	public static void init() {
		System.out.println("Proxy.init()");
		System.setProperty("http.proxyHost", "anbid-px01");
		System.setProperty("http.proxyPort", "8080");
	}

	public static final void main(String[] args) {
		init();
		testarURL("http://www.w3.org/2005/05/xmlmime");
		testarURL("https://ws.homologacao.sistemagalgo:7843");
		testarURL("http://www.springframework.org:7843");
		testarURL("http://cxf.apache.org");
		System.out.println("Fim");
	}

	private static void testarURL(String addr) {
		System.out.println("Testando " + addr);
		URL url;
		try {
			url = new URL(addr);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStream out = conn.getOutputStream();
			out.close();
			System.out.println("Acesso OK");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Proxy(String address, int port, String user, String password) {
		super();
		this.address = address;
		this.port = port;
		this.user = user;
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public int getPort() {
		return port;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}
}
