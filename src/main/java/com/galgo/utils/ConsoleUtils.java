package com.galgo.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsoleUtils {

	private static final Logger log = LoggerFactory.getLogger(ConsoleUtils.class);

	public static String getConsoleInput(String msg) {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.print(msg + " ");
		try {
			return in.readLine();
		} catch (IOException e) {
			log.error(null, e);
			return null;
		}
	}

	public static boolean trueIfYes(String string) {

		String resposta;
		do {
			resposta = getConsoleInput(string + " (y, n) ");
			if (!StringUtils.equalsIgnoreCase("y", resposta)
					&& !StringUtils.equalsIgnoreCase("n", resposta)) {
				log.error("A string {} nao eh valida, apenas y, Y, n ou N", resposta);
			} else {
				return StringUtils.equalsIgnoreCase("y", resposta);
			}
		} while (true);
	}

}
