package com.galgo.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Printer {
	
	private static final Logger log = LoggerFactory.getLogger(Printer.class);

	public static class Column {
		private String title;
		private String property;
		private String format;

		/**
		 * @param title
		 * @param property
		 * @param format
		 */
		public Column(String title, String property, String format) {
			this.title = title;
			this.property = property;
			this.format = format;
		}

		public String getTitle() {
			return title;
		}

		public String getProperty() {
			return property;
		}

		public String getFormat() {
			return format;
		}
	}

	private List<Printer.Column> cols = new ArrayList<Printer.Column>();

	public void addCol(Printer.Column col) {
		cols.add(col);
	}

	public void addCol(String title, String property, String format) {
		cols.add(new Column(title, property, format));
	}

	public void print(Collection<?> objs) {
		int i = 0;

		// Montar a String de formatacao
		StringBuffer fmtBuff = new StringBuffer("%3.3s");
		for (Printer.Column col : cols) {
			fmtBuff.append(" / ").append(col.getFormat());
		}
		String fmt = fmtBuff.append("\n").toString();

		// Montar o cabecalho
		List<String> titles = new ArrayList<String>();
		titles.add("order");
		for (Printer.Column col : cols) {
			titles.add(col.getTitle());
		}

		StringBuffer buff = new StringBuffer(String.format(fmt, titles.toArray()));
		for (Object obj : objs) {
			List<Object> objs_ = new ArrayList<Object>();
			objs_.add(i++);
			for (Printer.Column col : cols) {
				objs_.add(PropertyUtils.getPropertyDN(obj, col.getProperty()));
			}
			buff.append(String.format(fmt, objs_.toArray()));
		}
		log.info("\n" + buff.toString());
	}
	
	   public static void main(String[] args) {
	        Printer p = new Printer();
	        p.addCol("aaa", "nome", "%10.10s");
	        
	        Map<String, String> m = new HashMap<String, String>();
	        m.put("nome", "Valdemar");
	        List<Map<String, String>> l = new ArrayList<Map<String, String>>();
	        l.add(m);
	        
	        p.print(l);
	    }

}