/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils;

import java.util.Comparator;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Classe que implementa a interface java.util.Comparator para XMLGregoriaCalendar
 * @author valdemar.arantes
 */
public class XMLGregorianCalendarComparator implements Comparator<XMLGregorianCalendar> {

    public int compare(XMLGregorianCalendar o1, XMLGregorianCalendar o2) {
        if (o1 == null)
            return 1;
        if (o2 == null)
            return -1;

        return o1.compare(o2);
    }

}
