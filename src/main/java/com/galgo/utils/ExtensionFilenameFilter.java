package com.galgo.utils;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Implementacao de {@link FilenameFilter} que filtra pela extensao passada no construtor
 * @author valdemar.arantes
 *
 */
public class ExtensionFilenameFilter implements FilenameFilter {
	
	private String extension;
	
	/**
	 * @param extension
	 */
	public ExtensionFilenameFilter(String extension) {
		this.extension = extension.toLowerCase();
	}

	public boolean accept(File dir, String name) {
		return(name.toLowerCase().endsWith(extension));
	}

}
