package com.galgo.utils.sec;

public class CryptoException extends RuntimeException {

    private static final long serialVersionUID = 3357844615031867885L;

    public CryptoException() {
        // TODO Auto-generated constructor stub
    }

    public CryptoException(String message) {
        super(message);
    }

    public CryptoException(Throwable cause) {
        super(cause);
    }

    public CryptoException(String message, Throwable cause) {
        super(message, cause);
    }

    public CryptoException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
