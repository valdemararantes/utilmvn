package com.galgo.utils.sec;

import java.security.Key;

/**
 * Uma interface de chave
 * Ver <a href="http://www.ibm.com/developerworks/br/library/j-javadev2-23/index.html">
 * artigo da IBM</a>
 *  
 * @author valdemar.arantes
 *
 */
public interface CryptoKeyable {
    Key getKey();
}
