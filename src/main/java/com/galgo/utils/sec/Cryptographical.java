package com.galgo.utils.sec;

/**
 * Interface de criptografia genérica.
 * Ver <a href="http://www.ibm.com/developerworks/br/library/j-javadev2-23/index.html">
 * artigo da IBM</a>
 *  
 * @author valdemar.arantes
 *
 */
public interface Cryptographical {
    String encrypt(String plaintext);

    String decrypt(String ciphertext);
}
