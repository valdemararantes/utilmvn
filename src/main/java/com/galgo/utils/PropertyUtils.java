package com.galgo.utils;

import org.apache.commons.lang.StringUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;

/**
 * Invoca org.apache.commons.beanutils.PropertyUtils.getProperty, retornando nulo sem lancar excecao
 * no caso de alguma propriedade da cadeia seja nula.
 *
 * @author valdemar.arantes
 */
public class PropertyUtils extends org.apache.commons.beanutils.PropertyUtils {

    /**
     * Invocar org.apache.commons.beanutils.PropertyUtils.getProperty retornando valor Nulo se
     * ocorrer uma exceção.
     *
     * @param bean
     * @param name
     * @return
     */
    public static Object getPropertyDN(Object bean, String name) {
        try {
            return org.apache.commons.beanutils.PropertyUtils.getProperty(bean, name);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Insntancia as propriedades intermediárias se necessário
     *
     * @param bean
     * @param name
     * @param value
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    public static void setProperty(Object bean, String name, Object value) throws
            IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        instantiateNestedProperties(bean, name);
        org.apache.commons.beanutils.PropertyUtils.setProperty(bean, name, value);
    }

    public static void instantiateNestedProperties(Object obj, String fieldName) {
        try {
            String[] fieldNames = fieldName.split("\\.");
            if (fieldNames.length > 1) {
                StringBuffer nestedProperty = new StringBuffer();
                for (int i = 0; i < fieldNames.length - 1; i++) {
                    String fn = fieldNames[i];
                    if (i != 0) {
                        nestedProperty.append(".");
                    }
                    nestedProperty.append(fn);
                    Object value = PropertyUtils.getProperty(obj, nestedProperty.toString());

                    if (value == null) {
                        PropertyDescriptor propertyDescriptor = PropertyUtils.getPropertyDescriptor(
                                obj, nestedProperty.toString());
                        Class<?> propertyType = propertyDescriptor.getPropertyType();
                        Object newInstance = propertyType.newInstance();
                        PropertyUtils.setProperty(obj, nestedProperty.toString(), newInstance);
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * @param property Propriedade aninhada
     * @return Retorna a propriedade aninhada removendo todos os simbolos relacionadao
     * a propriedades mapeadas e indexadas. Ex: pessoa.parentes[0].nome retorna
     * pessoa.parentes.nome
     */
    public static String simplifyProperty(String property) {
        String pattern = "\\[.*?\\]";
        return property.replaceAll(pattern, "");
    }

    /**
     * @param property
     * @param indexPosition
     * @return Retorna a n-ésima posição de índice de uma propriedade indexada.
     * Exemplo: property=p.a[0].b[0].c.d[0].e index=1 ret=p.a[0]
     */
    public static String getIndexPropertyName(String property, int indexPosition) {

        if (StringUtils.isBlank(property)) {
            return "";
        }

        int startPos = 0;
        int endPos = 0;


        for (int i = 0; i < indexPosition; i++) {
            startPos = StringUtils.indexOf(property, '[', endPos);
            if (startPos == -1) {
                throw new IndexOutOfBoundsException("Propriedade " + property +
                        " não possui índice de posição " + (i + 1));
            }
            endPos = StringUtils.indexOf(property, ']', startPos);
        }
        return StringUtils.substring(property, 0, endPos+1);
    }

//    public static void main(String[] args) {
//        System.out.println(simplifyProperty("pessoa.parentes[0].nome[aaa].primeiro"));
//        System.out.println(getIndexPropertyName("p.a[0].b[0].c.d[0].e", 3));
//    }
}
