/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe que marca o tempo de duração entre as invocações dos métodos start e end.<br/>
 * Máquina de estados desta classe:<br/>
 * <pre>
 *           RUNNING | PAUSED | STOPPED
 * RUNNING     -     | pause  |  stop
 * PAUSED    resume  |   -    |  stop
 * STOPPED     -     |   -    |   -
 * </pre>
 *
 * @author valdemar.arantes
 */
public class Cronometro {

    private static final Logger log = LoggerFactory.getLogger(Cronometro.class);
    private Duration duration = new Duration(0);
    private DateTime start;
    private DateTime lastPause;
    private DateTime lastResume;
    private DateTime end;
    private CronState state;

    public static enum CronState {

        RUNNING, PAUSED, STOPPED
    };
    private static final PeriodFormatter DAYS_HOURS_MIN_SEC_MILISEC = new PeriodFormatterBuilder()
            .appendDays()
            .appendSuffix("dia", " dias")
            //.appendSeparator(" and ")
            .appendMinutes()
            .appendSuffix("min")
            //.appendSeparator(" and ")
            .appendSeconds()
            .appendSuffix("s")
            .appendMillis3Digit()
            .appendSuffix("ms")
            .toFormatter();

    /**
     * Inicia a cronometragem
     *
     * @return Uma instância de Cronometro que deve ser utilizada para se invocar o método end
     */
    public static Cronometro start() {
        Cronometro cron = new Cronometro();
        cron.start = new DateTime();
        cron.lastResume = cron.start;
        cron.state = CronState.RUNNING;
        log.debug("Cronômetro iniciado em {}", cron.start);
        return cron;
    }

    /**
     * Pause na cronometragem. Só ocorre se o cronômetro estiver em RUNNING.
     *
     * @return
     */
    public Cronometro pause() {
        DateTime now = new DateTime();
        Duration lastElapsed;
        if (state.equals(CronState.RUNNING)) {
            lastElapsed = new Duration(lastResume, lastPause);
            state = CronState.PAUSED;
            lastPause = now;
            duration = duration.plus(lastElapsed);
            log.debug("Cronômetro pausado depois de {}",
                    DAYS_HOURS_MIN_SEC_MILISEC.print(lastElapsed.toPeriod()));
        }
        return this;
    }

    /**
     * Retoma a cronometragem pausada.
     *
     * @return
     */
    public Cronometro resume() {
        DateTime now = new DateTime();
        if (state.equals(CronState.PAUSED)) {
            state = CronState.RUNNING;
            lastResume = now;
            log.debug("Cronômetro - seguir cronometragem em {}", now);
        }
        return this;
    }

    /**
     * Encerra a cronometragem
     *
     * @return String com o tempo cronometrado
     * @deprecated Utilizar stop()
     */
    public String end() {
        return stop().toString();
    }

    public Cronometro stop() {
        DateTime now = new DateTime();
        if (!state.equals(CronState.STOPPED)) {
            log.debug("Cronômetro encerrado em {}", now);
            if (state.equals(CronState.RUNNING)) {
                end = now;
                duration = duration.plus(new Duration(lastResume, now));
            } else {
                end = lastPause;
                log.debug("Cronômetro estava pausado. Tempo final será o do último pause: {}", 
                        lastPause);
            }
            state = CronState.STOPPED;
        }
        return this;
    }

    public Duration getDuration() {
        return duration;
    }

    public DateTime getStart() {
        return start;
    }

    public DateTime getLastPause() {
        return lastPause;
    }

    public DateTime getEnd() {
        return end;
    }

    public DateTime getLastResume() {
        return lastResume;
    }

    @Override
    public String toString() {
        DateTime now = new DateTime();
        Duration d_ = duration;
        String ret = "";
        if (state.equals(CronState.RUNNING)) {
            d_ = d_.plus(new Duration(lastResume, now));
            ret = "Cronômetro rodando! ";
        } else if (state.equals(CronState.PAUSED)) {
            ret = "Cronômetro pausado! ";
        }
        return ret + DAYS_HOURS_MIN_SEC_MILISEC.print(d_.toPeriod());
    }
}
