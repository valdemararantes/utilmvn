/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class ClassUtils {

    private static final Logger log = LoggerFactory.getLogger(ClassUtils.class);

    /**
     * @param clazz
     * @param fieldName
     * @return true se o campo for uma propriedade do tipo Lista com tipo genérico na classe clazz
     */
    public static boolean isListField(Class<?> clazz, String fieldName) {
        try {
            Field f = clazz.getDeclaredField(fieldName);
            return f.getType().equals(List.class);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @param clazz
     * @param fieldName
     * @return Retorna o tipo genérico associado à declaração do campo fieldName
     */
    public static Class<?> getGenericTypeFromField(Class<?> clazz, String fieldName) {
        try {
            Field f = clazz.getDeclaredField(fieldName);
            Type tp = f.getGenericType();
            if (tp instanceof ParameterizedType) {
                ParameterizedType pt = (ParameterizedType) tp;
                Type[] actualTypes = pt.getActualTypeArguments();
                Class<?> aClass = (Class<?>) actualTypes[0];
                return aClass;
            } else {
                log.info("O campo {} não foi declarado com classe genérica na classe {}", fieldName,
                        clazz.getName());
                return null;
            }
        } catch (Exception e) {
            log.error(null, e);
            return null;
        }
    }

}
