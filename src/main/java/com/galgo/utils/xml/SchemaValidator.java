/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galgo.utils.xml;

import com.galgo.utils.ApplicationException;
import com.google.common.collect.Lists;
import java.io.CharConversionException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import javax.xml.XMLConstants;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Classe que valida um XML contra um XSD
 *
 * @author valdemar.arantes
 */
public class SchemaValidator {

    private static final Logger log = LoggerFactory.getLogger(SchemaValidator.class);
    private static final String XSD11_SCHEMA = "http://www.w3.org/XML/XMLSchema/v1.1";

    static {
//        System.setProperty(
//                "javax.xml.validation.SchemaFactory:http://www.w3.org/2001/XMLSchema/v1.1",
//                "org.apache.xerces.jaxp.validation.XMLSchema11Factory");

    }

    /**
     * Valida o XML em xmlFile utilizando o schema definido em xsdUrl com Schema 1.1
     * @param xmlFile
     * @param xsdUrl
     * @return
     */
    public static List<String> validate11(final File xmlFile, final URL xsdUrl) {
        return validate_(xmlFile, xsdUrl, XSD11_SCHEMA);
    }

    /**
     * Valida o XML em xmlFile utilizando o schema definido em xsdUrl com Schema 1.0
     * @param xmlFile
     * @param xsdUrl
     * @return
     */
    public static List<String> validate(final File xmlFile, final URL xsdUrl) {
        return validate_(xmlFile, xsdUrl, XMLConstants.W3C_XML_SCHEMA_NS_URI);
    }

    private static List<String> validate_(final File xmlFile, final URL xsdUrl, String schema_ns) {
        try {
            // Validações
            if (xmlFile == null) {
                throw new IllegalArgumentException("Argumento xmlFile é obrigatório ");
            }
            if (xsdUrl == null) {
                throw new IllegalArgumentException("Argumento xsdUrl é obrigatório ");
            }
            if (!xmlFile.isFile()) {
                throw new IllegalArgumentException(String.format(
                        "Argumento xmlFile %s não é um arquivo", xmlFile.getAbsolutePath()));
            }

            log.debug("*** Validando o schema do arquivo {}", xmlFile.getAbsolutePath());
//            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI
//                    + "/v1.1");
            SchemaFactory sf = SchemaFactory.newInstance(schema_ns);
            log.debug("JAR de {}: {}", sf.getClass().getSimpleName() + ".class", sf.getClass().
                    getResource(sf.getClass().getSimpleName() + ".class"));
            log.debug("Schema definido no arquivo {}", xsdUrl);
            Schema schema;
            try {
                schema = sf.newSchema(xsdUrl);
            } catch (SAXParseException e) {
                throw new ApplicationException("Erro no parse do arquivo XSD " + xsdUrl, e);
            } catch (SAXException e) {
                throw new ApplicationException("Erro encontrado na criação do schema", e);
            }
            javax.xml.validation.Validator validator = schema.newValidator();

            // preparing the XML file as a SAX source
            SAXSource source = null;
            try {
                source = new SAXSource(new InputSource(new FileInputStream(xmlFile)));
            } catch (FileNotFoundException e) {
                throw new ApplicationException("Arquivo " + xmlFile.getAbsolutePath()
                        + " não encontrado", e);
            }

            // validating the SAX source against the schema
            List<String> errors = Lists.newArrayList();
            validator.setErrorHandler(new MyErrorHandler(errors));
            try {
                /**
                 * Por conta do erro de formatação de mensagem quando o Locale é pt_BR (ver
                 * com.galgo.email.xml.SchemaValidatorTest.validate_regras_estrutura3_neto_usando_iso_reduzido)
                 */
                Locale originalLocale = Locale.getDefault();
                Locale.setDefault(Locale.ENGLISH);
                validator.validate(source);
                Locale.setDefault(originalLocale);
            } catch (SAXException e) {
                throw new ApplicationException("Erro encontrado na validação do arquivo XML:"
                        + "\n    " + e.getMessage(), e);
            } catch (CharConversionException e) {
                throw new ApplicationException("Erro encontrado na leitura do arquivo XML:\n    "
                        + "Erro na codificação dos caracteres (encoding)", e);
            } catch (IOException e) {
                throw new ApplicationException("Erro encontrado na leitura do arquivo XML:\n    "
                        + e.getMessage(), e);
            }
            return errors;
        } finally {
            log.debug("Fim da validação com o schema");
        }
    }

    private static class MyErrorHandler extends DefaultHandler {

        private final List<String> errors;

        private MyErrorHandler(List<String> errors) {
            this.errors = errors;
        }

        @Override
        public void warning(SAXParseException e) throws SAXException {
            log.warn("Linha " + e.getLineNumber() + "; Coluna " + e.getColumnNumber()
                    + "; Descrição: " + e.getMessage());
            printInfo(e);
        }

        @Override
        public void error(SAXParseException e) throws SAXException {
            printInfo(e);
        }

        @Override
        public void fatalError(SAXParseException e) throws SAXException {
            printInfo(e);
        }

        private void printInfo(SAXParseException e) {
            errors.add("Linha " + e.getLineNumber() + "; Coluna " + e.getColumnNumber()
                    + "; Descrição: " + e.getMessage());
        }
    }

}
