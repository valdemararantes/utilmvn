package com.galgo.utils.xml;

public class TagNotFoundException extends Exception {

	public TagNotFoundException() {
		super();
	}

	public TagNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public TagNotFoundException(String message) {
		super(message);
	}

	public TagNotFoundException(Throwable cause) {
		super(cause);
	}

}
