package com.galgo.utils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

/**
 * @since v3.0.0
 *
 * Extende a java.util.Properties simplificando a carga e o salvamento das propriedades
 * em arquivo.
 *
 * Created by valdemar.arantes on 19/05/2016.
 */
public class Config extends Properties {

    File configFile;

    /**
     * Cria um arquivo Properties carregando os dados do arquivo configFile
     */
    public Config(File configFile) {
        this.configFile = configFile;
        try {
            load(new FileReader(configFile));
        } catch (IOException e) {
            throw new ApplicationException(e);
        }
    }

    public Config(Reader configReader) {
        try {
            load(configReader);
        } catch (IOException e) {
            throw new ApplicationException(e);
        }
    }

    /**
     * Cria um arquivo Properties carregando os dados do arquivo configFile
     * sobrepostos aos valores defaults
     *
     * @param defaults the defaults.
     */
    public Config(Properties defaults, File configFile) {
        super(defaults);
        this.configFile = configFile;
        try {
            load(new FileReader(configFile));
        } catch (IOException e) {
            throw new ApplicationException(e);
        }
    }

    /**
     * Salva as propriedades em arquivo
     * @return Instância desta class (fluency pattern)
     */
    public Config save() {
        if (configFile == null) {
            return this;
        }

        try {
            store(new FileWriter(configFile), null);
            return this;
        } catch (IOException e) {
            throw new ApplicationException(e);
        }

    }
}
