package com.galgo.utils;

import com.google.common.collect.Lists;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.util.List;

/**
 * Created by valdemar.arantes on 16/03/2016.
 */
public class CodSTIParser {

    /**
     * Quebra uma String em uma lista de códigos STI usando como separadores quaisquer caracteres
     * que não sejam números e hífen.
     *
     * @param strCodigosSTI String contendo uma lista de códigos STI. Os códigos podem conter o hífen separador.
     * @return List de códigos STI
     * @throws ParseException
     */
    public static List<Integer> parse(String strCodigosSTI) throws ParseException {
        List<Integer> codSTIList = Lists.newArrayList();
        if (StringUtils.isBlank(strCodigosSTI)) {
            return codSTIList;
        }

        String[] codSTIStrArr = strCodigosSTI.split("[^0-9\\-]+");
        if (ArrayUtils.isNotEmpty(codSTIStrArr)) {
            for (int i = 0; i < codSTIStrArr.length; i++) {
                String cd = StringUtils.trimToNull(codSTIStrArr[i]);
                if (cd == null) {
                    continue;
                }
                cd = StringUtils.replace(cd, "-", "");
                if (!StringUtils.isNumeric(cd)) {
                    throw new ParseException("cdSTI=" + codSTIStrArr[i] + " não é válido", i);
                }
                codSTIList.add(Integer.valueOf(cd));
            }
        }
        return codSTIList;
    }
}
